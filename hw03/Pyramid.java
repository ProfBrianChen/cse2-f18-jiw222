////a program that prompts the user for the dimensions of a pyramid and returns the volume inside the pyramid

import java.util.Scanner;

public class Pyramid{
  //main method
  public static void main(String args[]){
    Scanner myScanner=new Scanner(System.in);
    System.out.print("The square side of the pyramid is (input length): ");
      double squareSide=myScanner.nextDouble();
    System.out.print("The height of the pyramid is (input height): ");
      double height=myScanner.nextDouble();
    
    double volumePyramid;
    double base;
    base=squareSide*squareSide;
    volumePyramid=base*height/3;
    
    System.out.println("The volume inside the pyramid is: "+volumePyramid);
    
    
  }
}