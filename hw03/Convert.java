///Hurricanes drop a tremendous amount of water on areas of land. 
///Write a program that asks the user for doubles that represent the number of acres of land 
////////affected by hurricane precipitation and how many inches of rain were dropped on average.  
////////Convert the quantity of rain into cubic miles. 

import java.util.Scanner;

public class Convert{
  //main method
  public static void main(String args[]){
    Scanner myScanner=new Scanner(System.in);
    System.out.print("Enter the affected area in acres: ");
    double affectedArea=myScanner.nextDouble();
    System.out.print("Enter the rainfall in the affected area in inches: ");
    double rainfall=myScanner.nextDouble();
    
    double rainfallMiles; 
    double areaMiles;
    double rainQuantity;
    rainfallMiles=rainfall/63360;     ///convert unit into miles
    areaMiles=affectedArea*0.00156;    ///convert unit into miles
    rainQuantity=rainfallMiles*areaMiles;
    
    System.out.println(rainQuantity+" cubic miles");
    
      
  }
}