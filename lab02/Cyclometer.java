//JIABEI WEI
//CSE 2, 311
//9/6/18 THUR
////TO PRINT NUMBER OF MINUTES FOR EACH TRIP,COUNTS FOR EACH TRIP
////TO PRINT DISTANCE OF EACH TRIP MILLAGE,DISTANCE FOR BOTH TRIPS

public class Cyclometer {
  //main method required for every Java program
  public static void main(String[] args) {
    
//input data

int secsTrip1=480; //number of seconds for trip 1
int secsTrip2=3220; //number of seconds for trip 2
int countsTrip1=1561; //number of counts (rotations) for trip 1
int countsTrip2=9037; //number of counts (rotations) for trip 2

double wheelDiameter=27.0; //number of wheel diameter
double PI=3.14159; //number of pi
double feetPerMile=5280; //convertion factor feet to mile
double inchesPerFoot=12; //convertion factor inches to foot
double secondsPerMinute=60; //convertion factor seconds to minutes
double distanceTrip1, distanceTrip2, totalDistance; //number of

System.out.println("Trip 1 took "+
                  (secsTrip1/secondsPerMinute)+" minutes and had "+
                   countsTrip1+" counts."); 
System.out.println("Trip 2 took "+
                  (secsTrip2/secondsPerMinute)+" minutes and had "+
                   countsTrip2+" counts."); 

//run the calculations; store the values. 
// 

distanceTrip1=countsTrip1*wheelDiameter*PI;
//Above gives distance in inches
//(for each count, a rotation of the wheel travels
//the diameter in inches times pi)
distanceTrip1=inchesPerFoot*feetPerMile; //Gives distance in miles
distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
totalDistance=distanceTrip1+distanceTrip2;

//Print out the output data.
System.out.println("Trip 1 was "+distanceTrip1+" miles");
System.out.println("Trip 2 was "+distanceTrip2+" miles");
System.out.println("The total distance was "+totalDistance+" miles");

                          } //end of main method
} //end of class
