// cse2, 311
// JiaBei Wei

import java.util.Scanner;
public class RemoveElements{
  public static void main(String[] arg){
	Scanner scan=new Scanner(System.in);
  int num[]=new int[10];
  int newArray1[];
  int newArray2[];
  int index = 0,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
    int c = 0;
    while ( c == 0 )
    {
      System.out.print("Enter the index ");
    	index = scan.nextInt();
      if ( index < num.length && index > 0 )
      {
        c++;
      }
    }
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
    System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
  	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
    String out="{";
    for(int j=0;j<num.length;j++){
      if(j>0){
        out+=", ";
      }
      out+=num[j];
    }
    out+="} ";
    return out;
    }
  

  
  public static int[] randomInput(){
    int[] array = new int[10];
    for ( int i = 0; i < 10; i++ )
    {
      int a = (int)(Math.random()*9);
      array[i] = a;
    }
    return array;
  }
  
  public static int[] delete( int[] list, int pos ){
    int[] listD = new int[list.length - 1];
    for ( int i = 0; i < pos; i++ )
    {
      listD[i] = list[i];
    }
    for ( int p = pos; p < listD.length; p++ )
    {
      listD[p] = list[p+1];
    }
    return listD;
  }
  
  public static int[] remove( int[] list, int target ){
    int c = 0;
    for ( int i = 0; i < list.length; i++ )
    {
      if ( list[i] == target)
      {
        c++;
      }
    }
    int n = list.length - c;
    int p = 0;
    int i = 0;
    while ( i < n )
    {
      if ( list[i] == target )
      {
        list = delete(list,i);
        i--;
      }
      i++;
    }
    return list;
  }
  
  
}