//JiaBei Wei
//homework 9
//11-27,2018

import java.util.Scanner;
public class CSE2Linear{
  
  public static void main(String[] args){
    //declare main method
    Scanner scan = new Scanner(System.in);
    //declare scan as name of user input
    int[] grades = new int[15];
      // create array of 15 called grades
    System.out.println("Enter 15 ascending integers for final grades in CSE2: ");
    for ( int i = 0; i < 15; i++ )
      //prompt and assign user input into array
    {
      int c = 0;
      while ( c == 0 )
      {
        if ( scan.hasNextInt() ) //check if input is an integer
        {
          int b = scan.nextInt();
          if ( b > 100 || b < 0 ) //check if input out of range
          {
            System.out.println("The integer entered is out of range (0-100)");
          }
          else 
          {
            grades[i] = b;
            if ( i > 0 )
            {
              if ( grades[i] < grades[i-1] ) //check if input smaller than last one
              {
                System.out.println("The integer is smaller than the last one. ");
              }
              else
              {
                c++;
              }
            }
            else 
            {
              c++;
            }
          }
        }
        else 
        {
          System.out.println("Please enter an integer. ");
        }
      }
    }
    //print out the array
    print( grades );
    //prompt to enter a grade to search for
    int c = 0, g = 0;
    while ( c == 0 )
    {
      System.out.println("Enter a grade to search for: ");
      if ( scan.hasNextInt() )
      {
        g = scan.nextInt();
        c++;
      }
    }
    int search = binarySearch( grades, g );
    if ( search > 0 )
    {
      System.out.println( g + " was found in the list with " + search + " iterations. ");
    }
    else 
    {
      System.out.println( g + " was not found in the list with 4 iterations. ");
    }
    scramble( grades );
    print( grades );
    //prompt to enter a grade to search for
    c = 0;
    g = 0;
    while ( c == 0 )
    {
      System.out.println("Enter a grade to search for: ");
      if ( scan.hasNextInt() )
      {
        g = scan.nextInt();
        c++;
      }
    }
    search = linearSearch( grades, g );
    if ( search > 0 )
    {
      System.out.println( g + " was found in the list with " + search + " iterations. ");
    }
    else 
    {
      System.out.println( g + " was not found in the list with 15 iterations. ");
    }
    
  }
  
  public static void print( int[] array ){
    System.out.println("Grades: ");
    for ( int i = 0; i < 15; i++ )
    {
      System.out.print( array[i] + " ");
    }
    System.out.println(".");
  }
  
  public static int[] scramble( int[] grades ){
    for ( int s = 0; s < 15; s++ )
    {
      int a = (int)(Math.random()*14);
      int b = grades[0];
      grades[0] = grades[a];
      grades[a] = b;
    }
    System.out.println("Scrambled: ");
    return grades;
  }
  
  public static int linearSearch( int[] list, int key ){
    int iterations = 0;
    for ( int i = 0; i < list.length; i++ )
    {
      iterations++;
      if ( key == list[i] )
      {
        return iterations;
      }
    }
    return -1;
  }
  
  public static int binarySearch( int[] list, int key ){
    int low = 0;
    int high = list.length - 1;
    int iterations = 0;
    while ( high >= low )
    {
      int mid = ( low + high ) / 2;
      if ( key < list[mid] )
      { 
        high = mid - 1; 
      }
      else if ( key == list[mid] )
      {
        iterations++;
        return iterations;
      }
      else 
      {
        low = mid + 1;
      }
      iterations++;
    }
    return -1;
  }
  
  
}