//hw05
//Jiabei Wei
//cse2,311





import java.util.Scanner;

public class Hw05{
  //use main method
  public static void main(String[] args){
    Scanner scan=new Scanner(System.in);   
    //initialize hands with 0
      int hands=0;
    //initialize all cards with 0
    int ace=0, face2=0, face3=0, face4=0, face5=0, face6=0, face7=0, 
    face8=0, face9=0, face10=0, joker=0, queen=0, king=0;
    //assign value to number of hands generating
    while (hands==0)
    {
      //ask the user how many times it should generate hands
      System.out.print("How many hands do you want to generate?\n");
      if ( scan.hasNextInt() )
      {
        //record the number of hands
        //or prompt the user to enter valid number
        hands=scan.nextInt();
        if ( hands<0 )
        {
          hands=0;
          System.out.print("Invalid input\n"+"Enter another integer\n");
        }
        //get out of the loop if everything is correct
        else
        {
          break;
        }
      }
      //if not integer entered, prompt user to do it again
      else
      {
        scan.next();
        hands=0;
        System.out.print("Invalid input\n"+"Enter another integer\n");
      }
      break;
    }
    
    //initialize useful values as 0
    int count=0, countCards=0, rank;
    int four=0, three=0, two=0, one=0;
    //start counting number of hands and generating cards until reached the value entered
    while ( count!=hands )  
    {
      count++;
      //generate card 1 of count hand#
      int card1=(int)(Math.random()*52)+1;
        //check rank
        rank=card1%13;
    //record rank of the card
        switch ( rank )
        {
          case 1:
            ace++;
            break;
          case 2:
            face2++;
            break;
          case 3:
            face3++;
            break;
          case 4:
            face4++;
            break;
          case 5:
            face5++;
            break;
          case 6:
            face6++;
            break;
          case 7:
            face7++;
            break;
          case 8:
            face8++;
            break;
          case 9:
            face9++;
            break;
          case 10:
            face10++;
            break;
          case 11:
            joker++;
            break;
          case 12:
            queen++;
            break;
          case 0:
            king++;
            break;
        }
      //generate card 2
      int card2=(int)(Math.random()*52)+1;
      //check if it was the same as the one before
      //if yes, generate another card number
      while (card2==card1)
      {
        card2=(int)(Math.random()*52)+1;
      }
      //check rank
        rank=card2%13;
   //record it
       switch ( rank )
        {
          case 1:
            ace++;
            break;
          case 2:
            face2++;
            break;
          case 3:
            face3++;
            break;
          case 4:
            face4++;
            break;
          case 5:
            face5++;
            break;
          case 6:
            face6++;
            break;
          case 7:
            face7++;
            break;
          case 8:
            face8++;
            break;
          case 9:
            face9++;
            break;
          case 10:
            face10++;
            break;
          case 11:
            joker++;
            break;
          case 12:
            queen++;
            break;
          case 0:
            king++;
            break;
        }
      //generate card 3
      int card3=(int)(Math.random()*52)+1;
      //check if repeated
      while (card3==card1||card3==card2)
      {
        card3=(int)(Math.random()*52)+1;
      }
      //check rank
        rank=card3%13;
   //record it
        switch ( rank )
        {
          case 1:
            ace++;
            break;
          case 2:
            face2++;
            break;
          case 3:
            face3++;
            break;
          case 4:
            face4++;
            break;
          case 5:
            face5++;
            break;
          case 6:
            face6++;
            break;
          case 7:
            face7++;
            break;
          case 8:
            face8++;
            break;
          case 9:
            face9++;
            break;
          case 10:
            face10++;
            break;
          case 11:
            joker++;
            break;
          case 12:
            queen++;
            break;
          case 0:
            king++;
            break;
        }
      //generate card 4
      int card4=(int)(Math.random()*52)+1;
      //check if repeated
      while (card4==card1||card4==card2||card4==card3)
      {
        card4=(int)(Math.random()*52)+1;
      }
      //check rank
        rank=card4%13;
   //record it
        switch ( rank )
        {
          case 1:
            ace++;
            break;
          case 2:
            face2++;
            break;
          case 3:
            face3++;
            break;
          case 4:
            face4++;
            break;
          case 5:
            face5++;
            break;
          case 6:
            face6++;
            break;
          case 7:
            face7++;
            break;
          case 8:
            face8++;
            break;
          case 9:
            face9++;
            break;
          case 10:
            face10++;
            break;
          case 11:
            joker++;
            break;
          case 12:
            queen++;
            break;
          case 0:
            king++;
            break;
        }
      //generate card 5
      int card5=(int)(Math.random()*52)+1;
      //check if repeated
      while (card5==card1||card5==card2||card5==card3||card5==card4)
      {
        card5=(int)(Math.random()*52)+1;
      }
      //check rank
        rank=card5%13;
   //record it
        switch ( rank )
        {
          case 1:
            ace++;
            break;
          case 2:
            face2++;
            break;
          case 3:
            face3++;
            break;
          case 4:
            face4++;
            break;
          case 5:
            face5++;
            break;
          case 6:
            face6++;
            break;
          case 7:
            face7++;
            break;
          case 8:
            face8++;
            break;
          case 9:
            face9++;
            break;
          case 10:
            face10++;
            break;
          case 11:
            joker++;
            break;
          case 12:
            queen++;
            break;
          case 0:
            king++;
            break;
        }
      //end of one hand of cards
      
      
      
      //check if this hand is one of the four situations
      //starting by ace, then 2,3,4,5 and so on
      while ( ace>1 )
      {
        if (ace==4)
        {
          four++;
          break;
        }
        else if (ace==3)
        {
          three++;
          break;
        }
        else if (ace==2)
        {
          if (face2==2||face3==2||face4==2||face5==2||face6==2||face7==2||face8==2||face9==2||face10==2||joker==2||queen==2||king==2)
          {
            two++;break;
          }
          else if (face2<2||face3<2||face4<2||face5<2||face6<2||face7<2||face8<2||face9<2||face10<2||joker<2||queen<2||king<2)
          {
            one++;break;
          }
        }
      }
      
      while ( face2>1 )
      {
        if (face2==4)
        {
          four++;break;
        }
        else if (face2==3)
        {
          three++;break;
        }
        else if (face2==2)
        {
          if (ace==2||face3==2||face4==2||face5==2||face6==2||face7==2||face8==2||face9==2||face10==2||joker==2||queen==2||king==2)
          {
            two++;break;
          }
          else if (ace<2||face3<2||face4<2||face5<2||face6<2||face7<2||face8<2||face9<2||face10<2||joker<2||queen<2||king<2)
          {
            one++;break;
          }
        }
      }
      
      while ( face3>1 )
      {
        if (face3==4)
        {
          four++;break;
        }
        else if (face3==3)
        {
          three++;break;
        }
        else if (face3==2)
        {
          if (ace==2||face2==2||face4==2||face5==2||face6==2||face7==2||face8==2||face9==2||face10==2||joker==2||queen==2||king==2)
          {
            two++;break;
          }
          else if (ace<2||face2<2||face4<2||face5<2||face6<2||face7<2||face8<2||face9<2||face10<2||joker<2||queen<2||king<2)
          {
            one++;break;
          }
        }
      }
      
      while ( face4>1 )
      {
        if (face4==4)
        {
          four++;break;
        }
        else if (face4==3)
        {
          three++;break;
        }
        else if (face4==2)
        {
          if (ace==2||face2==2||face3==2||face5==2||face6==2||face7==2||face8==2||face9==2||face10==2||joker==2||queen==2||king==2)
          {
            two++;break;
          }
          else if (ace<2||face2<2||face3<2||face5<2||face6<2||face7<2||face8<2||face9<2||face10<2||joker<2||queen<2||king<2)
          {
            one++;break;
          }
        }
      }
      
      while ( face5>1 )
      {
        if (face5==4)
        {
          four++;break;
        }
        else if (face5==3)
        {
          three++;break;
        }
        else if (face5==2)
        {
          if (ace==2||face2==2||face3==2||face4==2||face6==2||face7==2||face8==2||face9==2||face10==2||joker==2||queen==2||king==2)
          {
            two++;break;
          }
          else if (ace<2||face2<2||face3<2||face4<2||face6<2||face7<2||face8<2||face9<2||face10<2||joker<2||queen<2||king<2)
          {
            one++;break;
          }
        }
      }
      
      while ( face6>1 )
      {
        if (face6==4)
        {
          four++;break;
        }
        else if (face6==3)
        {
          three++;break;
        }
        else if (face6==2)
        {
          if (ace==2||face2==2||face3==2||face5==2||face4==2||face7==2||face8==2||face9==2||face10==2||joker==2||queen==2||king==2)
          {
            two++;break;
          }
          else if (ace<2||face2<2||face3<2||face5<2||face4<2||face7<2||face8<2||face9<2||face10<2||joker<2||queen<2||king<2)
          {
            one++;break;
          }
        }
      }
      
      while ( face7>1 )
      {
        if (face7==4)
        {
          four++;break;
        }
        else if (face7==3)
        {
          three++;break;
        }
        else if (face7==2)
        {
          if (ace==2||face2==2||face3==2||face5==2||face6==2||face4==2||face8==2||face9==2||face10==2||joker==2||queen==2||king==2)
          {
            two++;break;
          }
          else if (ace<2||face2<2||face3<2||face5<2||face6<2||face4<2||face8<2||face9<2||face10<2||joker<2||queen<2||king<2)
          {
            one++;break;
          }
        }
      }
      
      while ( face8>1 )
      {
        if (face8==4)
        {
          four++;break;
        }
        else if (face8==3)
        {
          three++;break;
        }
        else if (face8==2)
        {
          if (ace==2||face2==2||face3==2||face5==2||face6==2||face7==2||face4==2||face9==2||face10==2||joker==2||queen==2||king==2)
          {
            two++;break;
          }
          else if (ace<2||face2<2||face3<2||face5<2||face6<2||face7<2||face4<2||face9<2||face10<2||joker<2||queen<2||king<2)
          {
            one++;break;
          }
        }
      }
      
      while ( face9>1 )
      {
        if (face9==4)
        {
          four++;break;
        }
        else if (face9==3)
        {
          three++;break;
        }
        else if (face9==2)
        {
          if (ace==2||face2==2||face3==2||face5==2||face6==2||face7==2||face8==2||face4==2||face10==2||joker==2||queen==2||king==2)
          {
            two++;break;
          }
          else if (ace < 2 || face2 < 2 ||face3<2||face5<2||face6<2||face7<2
                   ||face8<2||face4<2||face10<2||joker<2||queen<2||king<2)
          {
            one++;break;
          }
        }
      }
      
      while ( face10>1 )
      {
        if (face10==4)
        {
          four++;break;
        }
        else if (face10==3)
        {
          three++;break;
        }
        else if (face10==2)
        {
          if (ace==2||face2==2||face3==2||face5==2||face6==2||face7==2||face8==2||face9==2||face4==2||joker==2||queen==2||king==2)
          {
            two++;break;
          }
          else if (ace<2||face2<2||face3<2||face5<2||face6<2||face7<2||face8<2||face9<2||face4<2||joker<2||queen<2||king<2)
          {
            one++;break;
          }
        }
      }
      
      while ( joker>1 )
      {
        if (joker==4)
        {
          four++;break;
        }
        else if (joker==3)
        {
          three++;break;
        }
        else if (joker==2)
        {
          if (ace==2||face2==2||face3==2||face5==2||face6==2||face7==2||face8==2||face9==2||face10==2||face4==2||queen==2||king==2)
          {
            two++;break;
          }
          else if (ace<2||face2<2||face3<2||face5<2||face6<2||face7<2||face8<2||face9<2||face10<2||face4<2||queen<2||king<2)
          {
            one++;break;
          }
        }
      }
      
      while ( queen>1 )
      {
        if (queen==4)
        {
          four++;break;
        }
        else if (queen==3)
        {
          three++;break;
        }
        else if (queen==2)
        {
          if (ace==2||face2==2||face3==2||face5==2||face6==2||face7==2||face8==2||face9==2||face10==2||face4==2||joker==2||king==2)
          {
            two++;break;
          }
          else if (ace<2||face2<2||face3<2||face5<2||face6<2||face7<2||face8<2||face9<2||face10<2||face4<2||joker<2||king<2)
          {
            one++;break;
          }
        }
      }
      
      while ( king>1 )
      {
        if (king==4)
        {
          four++;break;
        }
        else if (king==3)
        {
          three++;break;
        }
        else if (king==2)
        {
          if (ace==2||face2==2||face3==2||face5==2||face6==2||face7==2||face8==2||face9==2||face10==2||face4==2||queen==2||joker==2)
          {
            two++;break;
          }
          else if (ace<2||face2<2||face3<2||face5<2||face6<2||face7<2||face8<2||face9<2||face10<2||face4<2||queen<2||joker<2)
          {
            one++;break;
          }
        }
      }
      
    }
    
    int twoFinal=((two%2)+(two-two%2)/2);

      
    
    //print out results
    System.out.println("number of loops ran: "+count);
    System.out.print("The probability for ");
    //cast integer count into a double
    double countDouble=(double)count;
    
    
    //calculate probability of each of the situations
  double fo=(double)(four/countDouble);
  double th=(double)(three/countDouble);
  double tw=(double)(twoFinal/countDouble);
  double on=(double)(one/countDouble);
  
    
    //print out results
      System.out.println("four of a kind: "+fo);
      System.out.println("three of a kind: "+th);
      System.out.println("two pairs: "+tw);
      System.out.println("one pair: "+on);  
  
  }
  
}
