// Jiabei Wei
// cse2, 311
// 2018,11,11

import java.util.Scanner;
public class hw08{ 
  public static void main(String[] args) { 
    Scanner scan = new Scanner(System.in); 
     //suits club, heart, spade or diamond 
    String[] suitNames={"C","H","S","D"};    
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
    String[] cards = new String[52]; 
    System.out.println("How many cards in a hand would you like? ");
    int numCards = 0;
    while ( numCards == 0 )
    {
      numCards = scan.nextInt();
      if ( numCards <= 0 || numCards >= 52 )
      {
        numCards = 0;
      }
    }
    String[] hand = new String[numCards]; 
    int again = 1; 
    int index = 51;
    for (int i=0; i<52; i++)
    { 
      cards[i] = rankNames[i%13] + suitNames[i/13]; 
    } 
    System.out.println();
    printArray(cards); 
    shuffle(cards); 
    printArray(cards); 
    while(again == 1)
    { 
       if ( index < numCards )
       {
         for (int i=0; i<52; i++)
         { 
           cards[i]=rankNames[i%13]+suitNames[i/13]; 
           System.out.print(cards[i]+" "); 
         } 
         shuffle(cards); 
         printArray(cards);
         index = 51;
       }
       hand = getHand(cards,index,numCards); 
       System.out.println("hands = ");
       printArray(hand);
       index = index - numCards;
       System.out.println("Enter a 1 if you want another hand drawn"); 
       again = scan.nextInt(); 
    }  
  } 
  
  public static void printArray(String[] list){
    for ( int i = 0; i < list.length; i++ )
    {
      System.out.print(list[i]+" "); 
    }
    System.out.println();
  }
  
  public static String[] shuffle(String[] list){
    for ( int s = 0; s < 55; s++ )
    {
      int a = (int)(Math.random()*52);
      String b = list[0];
      list[0] = list[a];
      list[a] = b;
    }
    System.out.println("\n" + "Shuffled ");
    return list;
  }
  
   public static String[] getHand(String[] list, int index, int numCards){
     String[] hand = new String[numCards];
     for ( int n = 0; n < numCards; n++ ){
       hand[n] = list[ index - n ];
     }
     return hand;
   }
}
