//Jiabei Wei
//homework06
//cse002,311
///10-23,Saturday


//import java scanner class
import java.util.Scanner;

//start up a public class called EncryptedX
public class EncryptedX{
  //use the main method
  public static void main(String args[]){
    
    //declare scan as object of the scanner class, input will be given to system
    Scanner scan = new Scanner(System.in);
    //initialize integer value for size as zero
    int size = 0, test = 0;
    //validate input value
    while ( test == 0 )
    {
      System.out.print("Enter an integer between 0 to 100: ");
      if ( scan.hasNextInt() )
      {
        size = scan.nextInt();
        if ( size >= 0 & size <=100 )
        {
          test++;
        }
        else
        {
          scan.next();
          System.out.println("Please enter another valid integer FROM 0 TO 100! ");
        }
      }
      else
      {
        scan.next();
        System.out.println("Please enter another VALID INTEGER between 0 to 100! ");
      }
    }
    
    //to print the encrypted x line
    //initialize variable line 
    String line = "";
    //set up variable i for number of horizontal lines
    // variable n for characters in each line
    int i, n; 
    for ( i = 1; i <= ( size + 1 ); i++ )
    {
      for ( n = 1; n <= ( size + 1 ); n++ )
      {
        if ( n == i | ( size + 2 - i ) == n )
        {
          line += " ";
        }
        else 
        {
          line += "*";
        }
      }
      System.out.println( line );
      line = "";
    }
    
    
    
    
  }
}