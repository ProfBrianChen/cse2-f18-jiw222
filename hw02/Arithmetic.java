/////JIABEI WEI
//CSE 2, 311
///HW02
//9-11-18, TUESDAY
//OBJECTIVE: practice manipulating data stored in variables, in running simple calculations, and in printing the numerical output that you generated.


public class Arithmetic {
      //using main method; main method required for every Java program
  public static void main(String args[]){

    //number of pairs of pants
    int numPants=3;
    //cost per pair fo pants
    double pantsPrice=34.98; 
    
    //number of sweatshirts
    int numShirts=2;
    //cost per shirt
    double shirtPrice=24.99;
    
    //number of belts
    int numBelts=1; 
    //cost per belts
    double beltCost=33.99;
    
    //the tax rate
    double paSalesTax=0.06;
    
    System.out.println("The total cost of pants "+
                      (numPants*pantsPrice)+" $ and tax of "+
                       Math.round((paSalesTax*numPants*pantsPrice))*100/100.00+" $");
    System.out.println("The total cost of shirts "+
                      (numShirts*shirtPrice) +" $ and tax of "+
                      Math.round((paSalesTax*(numShirts*shirtPrice)))*100/100.00+"$");
      
    System.out.println("The total cost of bets "+
                      (numBelts*beltCost) +" $ and tax of "+
                      Math.round((paSalesTax*(numBelts*beltCost)))*100/100.00+" $");
      
    //total cost of purchases before tax
      double totalcostbt=(numPants*pantsPrice)+(numShirts*shirtPrice)+(numBelts*beltCost);
    //total sale tax
      double totaltax=paSalesTax*totalcostbt;
    //total paid
      double totalPaid=totaltax+totalcostbt;
    
    
    
      
      System.out.print("The total cost of purchases(before tax) is"+
                      (totalPaid)+"$");
      
  
  
                          }//end of main method
  
} //end of class
