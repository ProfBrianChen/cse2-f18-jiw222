///JiaBei Wei
///09-25, Tuesday
///CSE2,311

//   Ask the user if they would like randomly cast dice or if they would like to 
//       state the two dice they want to evaluate.
//   If they want randomly cast dice, generate two random numbers from 1-6, 
//       inclusive, representing the outcome of the two dice cast in the game.
//   If they want user provided dice, use the Scanner twice to ask the user 
//       for two integers from 1-6, inclusive.  Assume the user always provides 
//       an integer, but check if the integer is within the range.
//   Determine the slang terminology of the outcome of the roll
//   Print out the slang terminology
//   Exit.

import java.util.Scanner;

public class CrapsIf{
  //use main method
  public static void main(String[] args){
    //   Ask the user if they like randomly cast dice or if they like to state the two dice they want to evaluate
    Scanner myScanner=new Scanner(System.in);
    System.out.print("Would you like randomly cast dice (1) "+ 
                     "or state the two dice you want to evaluate (2)?\n");
      int option=myScanner.nextInt();
    if ( option==1 ){
      int dice1=(int)((Math.random()*6)+1);
      int dice2=(int)((Math.random()*6)+1);
      if ( dice1+dice2==2 ){
        System.out.print("Snake Eyes\n");
      }
      else if ( dice1+dice2==3 ){
        System.out.print("Ace Deuce\n");
      }
      else if ( dice1+dice2==4 ){
        if ( dice1==dice2 ){
          System.out.print("Hard Four\n");
        }
        else
          System.out.print("Easy Four\n");
      }
      else if ( dice1+dice2==5 ){
          System.out.print("Easy Five\n");
      }
      else if ( dice1+dice2==6 ){
        if ( dice1==dice2 ){
          System.out.print("Hard Six\n");
        }
        else
          System.out.print("Easy Six\n");
      }
      else if ( dice1+dice2==7 ){
        System.out.print("Seven out\n");
      }
      else if ( dice1+dice2==8 ){
        if ( dice1==dice2 ){
          System.out.print("Hard Eight\n");
        }
        else 
          System.out.print("Easy Eight\n");
      }
      else if ( dice1+dice2==9 ){
        System.out.print("Nine\n");
      }
      else if ( dice1+dice2==10 ){
        if ( dice1==dice2 ){
          System.out.print("Hard Ten\n");
        }
        else
          System.out.print("Easy Ten\n");
      }
      else if ( dice1+dice2==11 ){
        System.out.print("Yo-leven\n");
      }
      else if ( dice1+dice2==12 ){
        System.out.print("Boxcars!");
      }
      
      
    }
    else if ( option==2 ){
      
      System.out.print("Enter an integer from 1-6 for the first dice: ");
      int dice1=myScanner.nextInt();
      System.out.print("Enter an integer from 1-6 for the second dice: ");
      int dice2=myScanner.nextInt();
      
      if ( dice1+dice2==2 ){
        System.out.print("Snake Eyes\n");
      }
      else if ( dice1+dice2==3 ){
        System.out.print("Ace Deuce\n");
      }
      else if ( dice1+dice2==4 ){
        if ( dice1==dice2 ){
          System.out.print("Hard Four\n");
        }
        else
          System.out.print("Easy Four\n");
      }
      else if ( dice1+dice2==5 ){
          System.out.print("Easy Five\n");
      }
      else if ( dice1+dice2==6 ){
        if ( dice1==dice2 ){
          System.out.print("Hard Six\n");
        }
        else
          System.out.print("Easy Six\n");
      }
      else if ( dice1+dice2==7 ){
        System.out.print("Seven out\n");
      }
      else if ( dice1+dice2==8 ){
        if ( dice1==dice2 ){
          System.out.print("Hard Eight\n");
        }
        else 
          System.out.print("Easy Eight\n");
      }
      else if ( dice1+dice2==9 ){
        System.out.print("Nine\n");
      }
      else if ( dice1+dice2==10 ){
        if ( dice1==dice2 ){
          System.out.print("Hard Ten\n");
        }
        else
          System.out.print("Easy Ten\n");
      }
      else if ( dice1+dice2==11 ){
        System.out.print("Yo-leven\n");
      }
      else if ( dice1+dice2==12 ){
        System.out.print("Boxcars!");
      }
      
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
  }
  

}
