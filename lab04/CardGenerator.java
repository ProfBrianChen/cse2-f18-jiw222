//Jiabei Wei
//CSE2, 311
//9-20,Thurs
///Use a random number generator to select a number from 1 to 52 (inclusive).  
///Each number represents one card, and the suits are grouped:  
///Cards [1-13 represent the diamonds], [14-26 represent the clubs], 
///then [hearts], then [spades].  
///In all suits, card identities ascend in step with the card number: 
///14 is the ace of clubs, 15 is the 2 of clubs, and 26 is the king of clubs.


public class CardGenerator{
  //main method
  public static void main(String args[]){
    ///assign random value btw 1-52 to card
    int card=(int)(Math.random()*52)+1;
      
      if ( card>=2 && card<=10)   ////for number cards of diamonds
        System.out.println(card+" of diamonds"); ////print out card name
    else if ( card>=15 && card<=23 )  ////for number cards of clubs
      System.out.println((card-13)+" of clubs");  ////print out card name
    else if ( card>=28 && card<=36 )   ////for number cards of hearts
      System.out.println((card-26)+" of hearts");  ////print out card name
    else if ( card>=41 && card<=49 )  ////for number cards of spades
      System.out.println((card-39)+" of spades");  ////print out card name
    else 
      switch(card){   ///for other card names
        case 1:
          System.out.println("Ace of diamonds");  ///print out name of the card special name
          break;
        case 11:
          System.out.println("Jack of diamonds");  ///print out name of the card special name
          break;
        case 12:
          System.out.println("Queen of diamonds");  ///print out name of the card special name
          break;
        case 13:
          System.out.println("King of diamonds");  ///print out name of the card special name
          break;
        case 14:
          System.out.println("Ace of clubs");  ///print out name of the card special name
          break;
        case 24:
          System.out.println("Jack of clubs");  ///print out name of the card special name
          break;
        case 25:
          System.out.println("Queen of clubs");  ///print out name of the card special name
          break;
        case 26:
          System.out.println("King of clubs");  ///print out name of the card special name
          break;
        case 27:
          System.out.println("Ace of hearts");  ///print out name of the card special name
          break;
        case 37:
          System.out.println("Jack of hearts");  ///print out name of the card special name
          break;
        case 38:
          System.out.println("Queen of hearts");  ///print out name of the card special name
          break;
        case 39:
          System.out.println("King of hearts");  ///print out name of the card special name
        case 40:
          System.out.println("Ace of spades");  ///print out name of the card special name
          break;
        case 50:
          System.out.println("Jack of spades");  ///print out name of the card special name
          break;
        case 51:
          System.out.println("Queen of spades");  ///print out name of the card special name
          break;
        case 52:
          System.out.println("King of spades");  ///print out name of the card special name
          break;
         
          
          
          
          
          
      }
    
    
    
    
    
  }
  
}