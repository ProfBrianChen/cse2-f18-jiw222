//JiaBei Wei
//cse2,311
//2018/11-15

import java.util.Scanner;
public class lab09{
  public static void main(String[] args){
    Scanner scan = new Scanner(System.in);
    int a = 0, k = 0;
    while ( k == 0 )
    {
      System.out.println("Enter an integer for length of your string: ");
      a = scan.nextInt();
      if ( a > 7 )
      {
        k++;
      }
    }
    int[] array0 = new int[a];
    for ( int n = 0; n < a; n++ )
    {
      System.out.println("Enter an integer to store in the string: ");
      int c = scan.nextInt();
      array0[n] = c;
    }
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    inverter(array0);
    System.out.println("array0: ");
    print(array0);
    inverter2(array1);
    System.out.println("array1: ");
    print(array1);
    int[] array3 = inverter2(array2);
    System.out.println("array3: ");
    print(array3);
    
    
  }
  
  public static int[] copy(int[] input){
    int[] copy = new int[input.length];
    for ( int n = 0; n < input.length; n++ )
    {
      copy[n] = input[n];
    }
    return copy;
  }
  
  public static void inverter(int[] input){
    int n = 0;
    if ( input.length % 2 == 0 )
    {
      n = ( input.length / 2 );
    }
    else
    {
      n = ( ( input.length - 1 ) / 2 );
    }
    for ( int m = 0; m < n; m++ )
    {
      int c = input[m];
      input[m] = input[( input.length - m - 1 )];
      input[( input.length - m - 1 )] = c;
    }
  }
  
  public static int[] inverter2(int[] input){
    int[] copy1 = copy(input);
    inverter( copy1 );
    return copy1;
  }
  
  public static void print(int[] array){
    for ( int i = 0; i < array.length; i++ )
    {
      System.out.println("array[" + i + "] = " + array[i] );
    }
  }
  
  
}