//Jiabei Wei
//cse2, 311
//homework07
//10-29,2018

import java.util.Scanner;

public class wordTools{
  //Implement a sampleText() method that prompts the user to enter a string of their choosing. 
  //Store the text in a string and output the string. 
  public static String sampleText(){
    System.out.println("Enter a sample text: ");
    Scanner scan = new Scanner(System.in);
    String sampleText = scan.nextLine();
    System.out.print("You entered: \n" + sampleText + "\n");
    return sampleText;
  }
  
  public static String printMenu(){
    String c, w, f, r, s, q;
    //print keys for user
    System.out.println("MENU " + "\n" + " c - Number of non-whitespace characters");
    System.out.println(" w - Number of words " + "\n" + " f - Find text");
    System.out.println(" r - Replace all !'s " + "\n" + " s - Shorten spaces");
    System.out.println(" q - Quit ");
    //initiate choice and k
    String choice = null;
    int k = 0;
    Scanner scan = new Scanner(System.in);
    while ( k == 0 )
    { 
      System.out.println("Choose an option: ");
      if ( scan.hasNext() )
      {  
        choice = scan.next();
        switch ( choice ){
          case "c": k++; break;
          case "w": k++; break;
          case "f": k++; break;
          case "r": k++; break;
          case "s": k++; break;
          case "q": k++; break;
          default: k = 0; break; 
        }
      }
      else{ k = 0; }
    }
    return choice;
  }
  
  public static int getNumOfNonWSCharacters(String text){
    int nonWSC = 0, WS = 0, n ;
    int length = text.length();
    for ( n = 0; n < length; n++ )
    {
      if ( text.charAt(n) == ' ' )
      { WS++; }
    }
    nonWSC = ( length - WS );
    System.out.print("Number of non-whitespace characters: ");
    System.out.println( nonWSC );
    return nonWSC;
  }
  
  public static int getNumOfWords(String text){
    int length = text.length();
    int n = 0, wordNum = 0;
    while ( n < length )
    {
      while ( n < length && text.charAt(n) != ' ' )
      {
        n++;
        if ( n < length && text.charAt(n) == ' ' )
        { wordNum++; n++; }
      }
      while ( n < length && text.charAt(n) == ' ' )
      { n++; }
    }
    System.out.print("Number of words: ");
    System.out.println(wordNum);
    return wordNum;
  }
  
  public static int findText(String user, String text){
    int length = text.length(), userLength = user.length(), m, n;
    int instance = 0, c = 0;
    for ( n = 0; n < length; n++ )
    {
      for ( m = 0; m < userLength; m++ )
      {
        if ( text.charAt(n) == user.charAt(m) && userLength <= ( length - n ) )
        { c++; 
          continue; }
        else { break; }
      }
    }
    instance = c;
    System.out.print("Word instances: ");
    System.out.println(instance);
    return instance;
  }
  
  public static String replaceExclamation(String text){
    int exclamation = 0 ;
    int length = text.length();
    String textNew = text.replace("!",".");
    System.out.println("Edited text: ");
    System.out.println(textNew);
    return textNew;
  }
  
  public static String shortSpace(String text){
    String newText = null;
    for ( int n = 0 ; n < text.length() ; n++ )
    {
      if ( text.charAt(n) == ' ' )
      {
        if ( text.charAt(n+1) == ' ' )
        {
          text = text.replace("  "," ");
        }
      }
    }
    System.out.println("Edited text: ");
    System.out.println(text);
    return text;
  }
  
  public static void main(String[] args){
    String c, w, f, r, s, q;
    int k = 0, l = 0; 
    String text = sampleText(), user = null;
    while ( k == 0 )
    {
      String option = printMenu();
      switch ( option ){
        case "c":
          getNumOfNonWSCharacters(text);
          break;
        case "w":
          getNumOfWords(text);
          break;
        case "f":
          Scanner scan = new Scanner(System.in);
          while ( l == 0 )
          {
            System.out.println("Enter a word or phrase to be found: ");
            user = scan.nextLine(); 
            if ( user.length() > 1 )
            { l++; } else { l = 0; }
          }
          findText(user, text);
          break;
        case "r":
          replaceExclamation(text);
          break;
        case "s":
          shortSpace(text);
          break;
        case "q":
          k++;
          break;
      }
        
    }
    
  }
  
}